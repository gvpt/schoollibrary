<?php
/**
 * User: admin
 * Date: 18.2.2014
 * Time: 17:44
 */

class borrowController {

	private $registry;

	public function __construct(Registry $registry) {
		$this->registry = $registry;
		$urlBits = $this->registry->getObject('url')->getURLBits();
		if ($this->registry->getObject('auth')->isLoggedIn()) {
			switch (isset($urlBits[1]) ? $urlBits[1] : '') {
				case 'me':
					$this->myBorrows();
					break;
				case 'edit':
					$this->editBorrow(intval($urlBits[2]));
					break;
				default:
					$this->borrowBook(intval($urlBits[1]));
					break;
			}
		}
		else {
			$this->registry->redirectURL($this->registry->buildURL(array()), 'Nemáte oprávnenie na žiadanú operáciu! Prosím prihláste sa.', 'alert');
		}
	}

	private function myBorrows() {
		$tags = array();
		$tags['title'] = "Moje knihy - " . $this->registry->getSetting('sitename');
		$this->registry->getObject('template')->buildFromTemplate('borrow/myBorrows');
		$this->registry->getObject('template')->replaceTags($tags);
		echo $this->registry->getObject('template')->parseOutput();
	}

	private function borrowBook($bookId) {
		if(!empty($_POST)) {
			//borrow stuff
		}
		else {
			$this->uiBorrowBook($bookId);
		}
	}

	private function uiBorrowBook($bookId) {
		require_once(FRAMEWORK_PATH . 'models/book.php');
		$book = new Book($this->registry, $bookId);
		if ($book->isValid()) {
			$data = $book->toArray();
			$tags = array();
			$tags['title'] = "Požičať knihu - " . $this->registry->getSetting('sitename');
			$tags['bookTitle'] = $data['title'];
			if (file_exists(FRAMEWORK_PATH . 'uploads/books/' . formatHelper::createFileName($data['title']) . '.png')) {
				$tags['bookFile'] = formatHelper::createFileName($data['title']);
			}
			else {
				$tags['bookFile'] = 'empty';
			}
			$tags['bookAuthor'] = $data['authorName'];
			$this->registry->getObject('template')->buildFromTemplate('borrow/borrowBook');
			$this->registry->getObject('template')->replaceTags($tags);
			echo $this->registry->getObject('template')->parseOutput();
		}
	}


} 