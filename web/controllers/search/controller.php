<?php
/**
 * User: admin
 * Date: 19.9.2013
 * Time: 22:50
 */

class searchController {

    private $registry;

    public function __construct(Registry $registry) {
        $this->registry = $registry;
        $urlBits = $this->registry->getObject('url')->getURLBits();
		switch(isset($urlBits[1]) ? $urlBits[1] : '') {
			case 'scan':
				$this->uiScanCode();
				break;
			default:
				$this->search($urlBits[1], intval($urlBits[2]));
				break;
		}
    }

	private function uiScanCode() {
		$tags = array();
		$tags['title'] = 'Načítať kód - ' . $this->registry->getSetting('sitename');
		$this->registry->getObject('template')->buildFromTemplate('search/readCode');
		$this->registry->getObject('template')->replaceTags($tags);
		echo $this->registry->getObject('template')->parseOutput();
	}

	private function search($q, $offset) {
		if(!empty($q) && !empty($offset)) {
			$offset--;
			require_once(FRAMEWORK_PATH . 'models/search.php');
			$search = new Search($this->registry);
			$pagination = $search->searchRecords($q, $offset);
			$result = '';
			if ($pagination->getNumRowsPage() > 0) {
				while ($row = $this->registry->getObject('db')->resultsFromCache($pagination->getCache())) {
					$tags = array();
					if (file_exists(FRAMEWORK_PATH . 'uploads/books/' . formatHelper::createFileName($row['bok_title']) . '.png')) {
						$tags['bookFile'] = formatHelper::createFileName($row['bok_title']);
					}
					else {
						$tags['bookFile'] = 'empty';
					}
					$tags['bookTitle'] = $row['bok_title'];
					$tags['bookAuthor'] = $row['aut_fullName'];
					$tags['bookAuthorUrl'] = $row['aut_url'];
					$tags['bookUrl'] = formatHelper::createUrlString($row['bok_title']);
					$this->registry->getObject('template')->buildFromTemplate('search/book', false);
					$this->registry->getObject('template')->replaceTags($tags);
					$result .= $this->registry->getObject('template')->parseOutput();
				}

				$pagOutput = '';
				$encodeQuery = urlencode($q);
				if ($pagination->isFirst()) {
					$pagOutput .= '<li class="arrow unavailable"><a href="">&laquo;</a></li>' . "\n";
				}
				else {
					$pagOutput .= '<li class="arrow"><a href="' . $this->registry->getSetting('siteurl') . "/search/$encodeQuery/" . ($pagination->getCurrentPage()-1) . '">&laquo;</a></li>' . "\n";
				}
				for ($i = 1;$i <= $pagination->getNumPages(); $i++) {
					if ($i == $pagination->getCurrentPage()) {
						$pagOutput .= '<li class="current"><a href="' . $this->registry->getSetting('siteurl') . "/search/$encodeQuery/" . ($i) . '">' . $i . '</a></li>' . "\n";
					}
					else {
						$pagOutput .= '<li><a href="' . $this->registry->getSetting('siteurl') . "/search/$encodeQuery/" . ($i) . '">' . $i . '</a></li>' . "\n";
					}
				}
				if ($pagination->isLast()) {
					$pagOutput .= '<li class="arrow unavailable"><a href="">&raquo;</a></li>' . "\n";
				}
				else {
					$pagOutput .= '<li class="arrow"><a href="' . $this->registry->getSetting('siteurl') . "/search/$encodeQuery/" . ($pagination->getCurrentPage()+1) . '">&raquo;</a></li>' . "\n";
				}
			}
			else {
				$result .= '<div class="panel text-center">Žiadne výsledky pre zadaný reťazec.</div>' . "\n";
				$pagOutput = '';
			}
			$tags = array();
			$tags['title'] = $this->registry->getSetting('sitename');
			$tags['query'] = $q;
			$tags['result'] = $result;
			$tags['pagination'] = $pagOutput;
			$tags['resultCount'] = $pagination->getNumRowsPage();
			$this->registry->getObject('template')->buildFromTemplate('search/search');
			$this->registry->getObject('template')->replaceTags($tags);
			echo $this->registry->getObject('template')->parseOutput();
		}
		else {
			$tags = array();
			$tags['query'] = '';
			$tags['result'] = '';
			$tags['pagination'] = '';
			$tags['resultCount'] = 0;
			$this->uiSearch($tags);
		}
	}

	private function uiSearch($tags) {
		$tags['title'] = $this->registry->getSetting('sitename');
		$this->registry->getObject('template')->buildFromTemplate('search/search');
		$this->registry->getObject('template')->replaceTags($tags);
		echo $this->registry->getObject('template')->parseOutput();
	}
}