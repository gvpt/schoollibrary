<?php
class defaultController {
	private $registry;
	
	public function __construct(Registry $registry) {
		$this->registry = $registry;
		$urlBits = $this->registry->getObject('url')->getURLBits();
		switch(isset($urlBits[1]) ? $urlBits[1] : '') {
			default:
				$this->uiIndex();
				break;
		}
	}
	
	private function uiIndex() {
		$tags = array();
		$tags['title'] = $this->registry->getSetting('sitename');
		require_once(FRAMEWORK_PATH . 'models/books.php');
		$books = new Books($this->registry);
		$tags['numBooks'] = $books->numBooks();
		$this->registry->getObject('template')->buildFromTemplate('index');
		$this->registry->getObject('template')->replaceTags($tags);
		echo $this->registry->getObject('template')->parseOutput();
	}
}
?>