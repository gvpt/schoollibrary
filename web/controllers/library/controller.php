<?php
class libraryController {

	private $registry;
	
	public function __construct(Registry $registry) {
		$this->registry = $registry;
		$urlBits = $this->registry->getObject('url')->getURLBits();
		switch (isset($urlBits[1]) ? $urlBits[1] : '') {
			case 'book':
				$this->bookDelegator();
				break;
			case 'author':
				$this->authorDelegator();
				break;
			default:
				$this->viewLibrary();
				break;
		}
	}

	private function viewLibrary() {
		header("Location: " . $this->registry->getSetting('siteurl') . '/search');
	}

	private function bookDelegator() {
		require_once(FRAMEWORK_PATH . 'controllers/library/bookDelegator.php');
		$delegator = new bookDelegator($this->registry);
	}

	private function authorDelegator() {
		require_once(FRAMEWORK_PATH . 'controllers/library/authorDelegator.php');
		$delegator = new authorDelegator($this->registry);
	}
}
?>