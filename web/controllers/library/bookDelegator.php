<?php
/**
 * User: admin
 * Date: 12.2.2014
 * Time: 18:14
 */

class bookDelegator {

	private $registry;

	public function __construct(Registry $registry) {
		$this->registry = $registry;
		$urlBits = $this->registry->getObject('url')->getURLBits();
		if (isset($urlBits[2])) {
			switch ($urlBits[2]) {
				case 'new':
					$this->newBook();
					break;
				case 'edit':
					$this->editBook(intval($urlBits[3]));
					break;

				default:
					$this->viewBook($urlBits[2]);
					break;
			}
		}
		else {
			header("Location: " . $this->registry->getSetting('siteurl') . '/search');
		}
	}

	private function newBook() {
		if ($this->registry->getObject('auth')->getUser()->isAdmin()) {
			require_once(FRAMEWORK_PATH . 'models/book.php');
			require_once(FRAMEWORK_PATH . 'libs/files/fileManager.php');
			require_once(FRAMEWORK_PATH . 'libs/images/imageManager.php');
			if (isset($_POST['newBook_title']) && ($_FILES['newBook_file']['size'] != 0)) {
				$newPath = FRAMEWORK_PATH . 'uploads/books/';
				if (fileManager::checkDir($newPath)) {
					$file = new fileManager();
					$fileNameArray = explode('.', $_FILES['newBook_file']["name"]);
					$ext = $fileNameArray[count($fileNameArray)-1];
					$file->setFileName(formatHelper::createFileName($_POST['newBook_title']) . '.' . $ext);
					$file->setFileMime($_FILES['newBook_file']["type"]);
					$file->setFileSize($_FILES['newBook_file']["size"]);
					$file->setFilePath($_FILES['newBook_file']["tmp_name"]);
					if ($file->isValid()) {
						$image = new imageManager();
						$image->loadFromFile($file->getFilePath());
						$image->exportToPng();
						$image->scaleImage(300,0);
						$image->save($newPath . formatHelper::createFileName($_POST['newBook_title']) . '.png');
						$book = new Book($this->registry);
						$book->setTitle($_POST['newBook_title']);
						$book->setAuthor($_POST['newBook_author']);
						$book->setISBN($_POST['newBook_isbn']);
						$book->setQuantity($_POST['newBook_quantity']);
						if ($book->save()) {
							$this->registry->redirectURL($this->registry->buildURL(array('library', 'book', formatHelper::createUrlString($_POST['newBook_title']))), "Kniha bola úspešne pridaná!", "success");
						}
						else {
							$this->registry->redirectURL($this->registry->buildURL(array('library', 'book', 'new')), 'Nastala chyba pri pridavani knihy', 'alert');
						}
					}
					else {
						echo "File is invalid";
					}
				}
				else {
					echo $newPath;
				}
			}
			elseif (isset($_POST['newBook_title'])) {
				$book = new Book($this->registry);
				$book->setTitle($_POST['newBook_title']);
				$book->setAuthor($_POST['newBook_author']);
				$book->setISBN($_POST['newBook_isbn']);
				$book->setQuantity($_POST['newBook_quantity']);
				if ($book->save()) {
					$this->registry->redirectURL($this->registry->buildURL(array('library', 'book', formatHelper::createUrlString($_POST['newBook_title']))), "Kniha bola úspešne pridaná!", "success");
				}
				else {
					$this->registry->redirectURL($this->registry->buildURL(array('library', 'book', 'new')), 'Nastala chyba pri pridavani knihy', 'alert');
				}
			}
			else {
				$this->uiNew();
			}
		}
		else {
			$this->registry->redirectURL($this->registry->buildURL(array()), 'Nemáte oprávnenie na žiadanú operáciu!', 'alert');
		}
	}

	private function uiNew() {
		$tags = array();
		$tags['title'] = "Pridať knihu - " . $this->registry->getSetting('sitename');
		$tags['authorList'] = $this->createAuthorDatalist();
		$this->registry->getObject('template')->buildFromTemplate('book/newBook');
		$this->registry->getObject('template')->replaceTags($tags);
		echo $this->registry->getObject('template')->parseOutput();
	}

	private function  createAuthorDatalist() {
		require_once(FRAMEWORK_PATH . 'models/authors.php');
		$result = '';
		$authors = new Authors($this->registry);
		$cache = $authors->listAuthors();
		if ($this->registry->getObject('db')->numRowsFromCache($cache) > 0) {
			while ($row = $this->registry->getObject('db')->resultsFromCache($cache)) {
				$result .= '<option value="' . $row['id_author'] . '">' . $row['fullName'] . '</option>' . "\n";
			}
		}
		else {
			$result = '<tr><td>Žiadny záznam</td></tr>>';
		}
		return $result;
	}

	private function editBook($id) {
	if (isset($_POST['editBook_title'])) {
		//edit stuff
	}
	else {
		$formData = array();
		$this->uiEdit($formData);
	}
}

	private function uiEdit($id) {
		$tags = array();
		$tags['title'] = "Upraviť knihu - " . $this->registry->getSetting('sitename');
		$this->registry->getObject('template')->buildFromTemplate('book/editBook');
		$this->registry->getObject('template')->replaceTags($tags);
		echo $this->registry->getObject('template')->parseOutput();
	}

	private function viewBook($url_title) {
		$url_title = formatHelper::createUrlString($url_title);
		require_once(FRAMEWORK_PATH . 'models/book.php');
		$this->registry->getObject('db')->executeQuery("SELECT id_book FROM book WHERE bok_url = '$url_title'");
		if ($this->registry->getObject('db')->numRows() == 1) {
			$row = $this->registry->getObject('db')->getRows();
			$book = new Book($this->registry, $row['id_book']);
			if ($book->isValid()) {
				$data = $book->toArray();
				$tags = array();
				$tags['title'] = $data['title'] . " - " . $this->registry->getSetting('sitename');
				if (file_exists(FRAMEWORK_PATH . 'uploads/books/' . formatHelper::createFileName($data['title']) . '.png')) {
					$tags['bookFile'] = formatHelper::createFileName($data['title']);
				}
				else {
					$tags['bookFile'] = 'empty';
				}
				$tags['bookTitle'] = $data['title'];
				$tags['bookAuthor'] = $data['authorName'];
				$tags['bookAuthorUrl'] = formatHelper::createUrlString($data['authorName']);
				$tags['bookISBN'] = $data['isbn'];
				$tags['bookId'] = $data['id'];
				$this->registry->getObject('template')->buildFromTemplate('book/viewBook');
				$this->registry->getObject('template')->replaceTags($tags);
				echo $this->registry->getObject('template')->parseOutput();
			}
			else {
				$this->registry->redirectURL($this->registry->buildURL(array()), 'Je nám ľuto ale niekde nastala chyba :(', 'alert');
			}
		}
		else {
			$this->registry->redirectURL($this->registry->buildURL(array()), 'Je nám ľuto ale niekde nastala chyba :(', 'alert');
		}
	}
}