<?php
/**
 * User: admin
 * Date: 12.2.2014
 * Time: 20:17
 */

class authorDelegator {

	private $registry;

	public function __construct(Registry $registry) {
		$this->registry = $registry;
		$urlBits = $this->registry->getObject('url')->getURLBits();
		if (isset($urlBits[2])) {
			switch ($urlBits[2]) {
				case 'new':
					$this->newAuthor();
					break;
				case 'edit':
					$this->editAuthor(intval($urlBits[3]));
					break;

				default:
					$this->viewAuthor($urlBits[2]);
					break;
			}
		}
		else {
			header("Location: " . $this->registry->getSetting('siteurl') . '/search'); //mozno list
		}
	}

	private function newAuthor() {
		if ($this->registry->getObject('auth')->getUser()->isAdmin()) {
			require_once(FRAMEWORK_PATH . 'models/author.php');
			require_once(FRAMEWORK_PATH . 'libs/files/fileManager.php');
			require_once(FRAMEWORK_PATH . 'libs/images/imageManager.php');
			if (isset($_POST['newAuthor_lastName']) && ($_FILES['newAuthor_file']['size']) != 0) {
				$newPath = FRAMEWORK_PATH . 'uploads/authors/';
				if (fileManager::checkDir($newPath)) {
					$file = new fileManager();
					$fileNameArray = explode('.', $_FILES['newAuthor_file']["name"]);
					$ext = $fileNameArray[count($fileNameArray)-1];
					$file->setFileName(formatHelper::createFileName($_POST['newAuthor_firstName'] . $_POST['newAuthor_lastName']) . '.' . $ext);
					$file->setFileMime($_FILES['newAuthor_file']["type"]);
					$file->setFileSize($_FILES['newAuthor_file']["size"]);
					$file->setFilePath($_FILES['newAuthor_file']["tmp_name"]);
					if ($file->isValid()) {
						$image = new imageManager();
						$image->loadFromFile($file->getFilePath());
						$image->exportToPng();
						$image->scaleImage(250,0);
						$image->save($newPath . formatHelper::createFileName($_POST['newAuthor_firstName'] . $_POST['newAuthor_lastName']) . '.png');
						$author = new Author($this->registry);
						$author->setFirstName($_POST['newAuthor_firstName']);
						$author->setLastName($_POST['newAuthor_lastName']);
						if ($author->save()) {
							$this->registry->redirectURL($this->registry->buildURL(array('library', 'author', formatHelper::createUrlString($_POST['newAuthor_firstName'] . '+' . $_POST['newAuthor_lastName']))), "Autor bol úspešne pridaný!", "success");
						}
						else {
							$this->registry->redirectURL($this->registry->buildURL(array('library', 'author', 'new')), 'Nastala chyba pri pridavani autora', 'alert');
						}
					}
					else {
						echo "File is invalid";
					}
				}
				else {
					echo "Invalid path";
				}
			}
			elseif (isset($_POST['newAuthor_lastName'])) {
				$author = new Author($this->registry);
				$author->setFirstName($_POST['newAuthor_firstName']);
				$author->setLastName($_POST['newAuthor_lastName']);
				if ($author->save()) {
					$this->registry->redirectURL($this->registry->buildURL(array('library', 'author', formatHelper::createUrlString($_POST['newAuthor_firstName'] . '+' . $_POST['newAuthor_lastName']))), "Autor bol úspešne pridaný!", "success");
				}
				else {
					$this->registry->redirectURL($this->registry->buildURL(array('library', 'author', 'new')), 'Nastala chyva pri pridávaní autora.', 'alert');
				}
			}
			else {
				$this->uiNew();
			}
		}
		else {
			$this->registry->redirectURL($this->registry->buildURL(array()), 'Nemáte oprávnenie na žiadanú operáciu!', 'alert');
		}
	}

	private function uiNew() {
		$tags = array();
		$tags['title'] = "Pridať autora - " . $this->registry->getSetting('sitename');
		$this->registry->getObject('template')->buildFromTemplate('author/newAuthor');
		$this->registry->getObject('template')->replaceTags($tags);
		echo $this->registry->getObject('template')->parseOutput();
	}

	private function editAuthor($id) {
		if (isset($_POST['editAuthor_lastName'])) {
			//create stuff
		}
		else {
			$formData = array();
			$this->uiEdit($formData);
		}
	}

	private function uiEdit($id) {
		$tags = array();
		$tags['title'] = "Upraviť autora - " . $this->registry->getSetting('sitename');
		$this->registry->getObject('template')->buildFromTemplate('author/editAuthor');
		$this->registry->getObject('template')->replaceTags($tags);
		echo $this->registry->getObject('template')->parseOutput();
	}

	private function createAuthorBookList($authorId) {
		require_once(FRAMEWORK_PATH . 'models/books.php');
		$result = '';
		$books = new Books($this->registry);
		$cache = $books->listBooksByAuthor($authorId);
		if ($this->registry->getObject('db')->numRowsFromCache($cache) > 0) {
			while ($row = $this->registry->getObject('db')->resultsFromCache($cache)) {
				$result .= '<tr data-redirect-url="' . $this->registry->getSetting('siteurl') . '/library/book/' . $row['bok_url'] . '"><td>' . $row['bok_title'] . '</td></tr>' . "\n";
			}
		}
		return $result;
	}

	private function viewAuthor($url_name) {
		$url_name = formatHelper::createUrlString($url_name);
		require_once(FRAMEWORK_PATH . 'models/author.php');
		$this->registry->getObject('db')->executeQuery("SELECT id_author FROM author WHERE aut_url = '$url_name'");
		if ($this->registry->getObject('db')->numRows() == 1) {
			$row = $this->registry->getObject('db')->getRows();
			$author = new Author($this->registry, $row['id_author']);
			if ($author->isValid()) {
				$data = $author->toArray();
				$tags = array();
				$tags['title'] = $author->getFullName() . " - " . $this->registry->getSetting('sitename');
				$this->registry->firephp->log(FRAMEWORK_PATH . 'uploads/authors/' . formatHelper::createFileName($author->getFullName())  . '.png');
				if (file_exists(FRAMEWORK_PATH . 'uploads/authors/' . formatHelper::createFileName($author->getFullName())  . '.png')) {
					$tags['authorFile'] = formatHelper::createFileName($author->getFullName());
				}
				else {
					$tags['authorFile'] = 'empty';
				}
				$tags['authorName'] = $author->getFullName();
				$tags['authorWiki'] = formatHelper::createWikiUrl($data['firstName'] . '_' . $data['lastName']);
				$tags['authorBooks'] = $author->numBooks();
				$tags['bookList'] = $this->createAuthorBookList($row['id_author']);
				$this->registry->getObject('template')->buildFromTemplate('author/viewAuthor');
				$this->registry->getObject('template')->replaceTags($tags);
				echo $this->registry->getObject('template')->parseOutput();
			}
			else {
				echo '404';
			}
		}
		else {
			echo '404';
		}
	}
}