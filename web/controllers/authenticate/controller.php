<?php
/*
 * 05.06.2013
 * Authentization controller v2.0
 * Autorizacny controller
 * CHANGELOG:
 * 	- v1.0 [21.09.2012]: createTime
 *	- v2.0 [05.06.2013]: GoogleAPI OAuth
*/
class AuthenticateController {

	private $registry;
	
	public function __construct(Registry $registry) {
		$this->registry = $registry;
		$urlBits = $this->registry->getObject('url')->getURLBits();
		if (isset($urlBits[1])) {
			switch($urlBits[1]) {
				case 'login':
					$this->login();
				break;
				case 'logout':
					$this->logout();
				break;
			}
		}
	}

	private function login() {
		if ($this->registry->getObject('auth')->isLoggedIn()) {
			$this->registry->getObject('log')->insertLog('SQL', 'INF', 'Authenticate', 'Užívateľ ' . $this->registry->getObject('auth')->getUser()->getFullName() . ' bol prihlásený');
			$this->registry->redirectURL($this->registry->buildURL(array()), 'Boli ste úspešne prihlásený', 'success');
		}
		else {
			header('Location: ' . filter_var($this->registry->getObject('google')->getGoogleClient()->createAuthUrl()), FILTER_SANITIZE_URL);
		}
		if (isset($_GET['code'])) {
			$this->registry->getObject('google')->getGoogleClient()->authenticate($_GET['code']);
			$_SESSION['lib_token'] = $this->registry->getObject('google')->getGoogleClient()->getAccessToken();
			header('Location: ' . filter_var($this->registry->getObject('url')->buildURL(array('authenticate', 'login')), FILTER_SANITIZE_URL));
			return;
		}
	}

	private function logout() {
		if ($this->registry->getObject('auth')->isLoggedIn()) {
			$this->registry->getObject('auth')->logout();
		}
		else {
			$this->registry->redirectURL($this->registry->buildURL(array('authenticate', 'login')), 'Pre odhlásenie by ste sa mali asi najprv prihlásiť ;)', 'alert');
		}
	}
}
?>