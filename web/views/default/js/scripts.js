$(document).ready(function() {

	$("table").delegate("tr[data-redirect-url]", 'click',function(){
		window.location = $(this).attr('data-redirect-url');
	});
	
    $(document).ajaxStart(function() {
        $('#loader').foundation('reveal', 'open');
    });

    $(document).ajaxStop(function() {
        $('#loader').foundation('reveal', 'close');
    });

	$("form").delegate("input[list]", 'change',function(){
		$("#authorNameStatus").html($("option[value='"+ $(this).val() +"']").html());
		//$(this).setCustomValidity()
	});

	$("#formSearch").submit(function(e){
		e.preventDefault();
		window.location = $(this).attr('action') + urlencode($("#searchQuery").val()) + '/1/';
	});
});

$(window).bind("load", function () {
	var footer = $("#footer");
	var pos = footer.position();
	var height = $(window).height();
	height = height - pos.top;
	height = height - footer.height();
	if (height > 0) {
		footer.css({
			'margin-top': height + 'px'
		});
	}
});

function urlencode (str) {
	return encodeURIComponent(str).replace(/%20/g,'+')
}