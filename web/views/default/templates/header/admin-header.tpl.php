<div class="sticky">
	<nav class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="{siteurl}"><img src="{siteurl}/views/{defaultView}/images/logo.png" style="height: 30px;" alt="{sitename}"/> Interaktívna knižnica GVPT</a></h1>
			</li>
			<li class="toggle-topbar"><a href="#"><img src="{siteurl}/views/{defaultView}/images/menu.png" alt="Menu"/></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<li><a href="{siteurl}/borrow/me">Moje knihy</a></li>
				<li class="has-dropdown">
					<a href="#">Administrácia</a>
					<ul class="dropdown">
						<li><a href="{siteurl}/library/book/new">Pridať knihu</a> </li>
						<li><a href="{siteurl}/library/author/new">Pridať autora</a> </li>
					</ul>
				</li>
				<li class="divider"></li>
				<li class="has-dropdown">
					<a href="#">{userFullName}</a>
					<ul class="dropdown">
						<li><a href="{siteurl}/authenticate/logout">Odhlásiť sa</a></li>
					</ul>
				</li>
			</ul>
		</section>
	</nav>
</div>