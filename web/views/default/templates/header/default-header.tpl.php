<div class="sticky">
	<nav class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="{siteurl}"><img src="{siteurl}/views/{defaultView}/images/logo.png" style="height: 30px;" alt="{sitename}"/> Interaktívna knižnica GVPT</a></h1>
			</li>
			<li class="toggle-topbar"><a href="#"><img src="{siteurl}/views/{defaultView}/images/menu.png" alt="Menu"/></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<li class="divider"></li>
				<li class="has-form">
					<a href="{loginUrl}" class="button success">Prihlásiť sa</a>
				</li>
			</ul>
		</section>
	</nav>
</div>