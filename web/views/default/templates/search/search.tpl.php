<section class="row">
	<div class="large-10 large-centered columns">
		<form name="formSearch" id="formSearch" method="get" action="{siteurl}/search/">
			<div class="row">
				<div class="large-10 large-centered columns">
					<div class="row">
						<div class="large-10 columns">
							<input type="text" id="searchQuery" name="searchQuery" placeholder="Sem zadajte knihu alebo autora" value="{query}" required style="font-size: x-large; padding: 8px; color: rgba(0,0,0,0.75);"/>
						</div>
						<div class="large-2 columns text-center">
							<button class="button small" style="margin-top: 8px;">Vyhľadať</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

<section class="row">
	<div class="large-12 columns">
		<header><h2>Výsledky</h2></header>
		<br />
		<ul class="large-block-grid-2">
			{result}
		</ul>
		<div class="pagination-centered">
			<hr />
			<ul class="pagination">
				{pagination}
			</ul>
		</div>
	</div>
</section>

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(['trackSiteSearch',
		"{query}",
		false,
		{resultCount}
	]);
	_paq.push(["enableLinkTracking"]);

	(function() {
		var u=(("https:" == document.location.protocol) ? "https" : "http") + "://gymmt.sk/piwik/";
		_paq.push(["setTrackerUrl", u+"piwik.php"]);
		_paq.push(["setSiteId", "2"]);
		var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
		g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Piwik Code -->