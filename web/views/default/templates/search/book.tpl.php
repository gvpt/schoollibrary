<li>
	<div class="small-3 columns">
		<img src="{siteurl}/uploads/books/{bookFile}.png" alt="{bookTitle}" />
	</div>
	<div class="small-9 columns">
		<a href="{siteurl}/library/book/{bookUrl}"><h3>{bookTitle}</h3></a>
		<a href="{siteurl}/library/author/{bookAuthorUrl}" style="display: inline-block; line-height: 24px;">
			<img src="{siteurl}/views/default/images/author.png" style="height: 16px; width: auto;"/>
			{bookAuthor}
		</a>
	</div>
</li>