<section class="row">
	<div class="large-12">
		<div class="row">
			<header class="large-12 columns">
				<h2>Načítať QR kód</h2>
			</header>
		</div>
		<div class="row">
			<div class="large-9 large-centered columns">
				<div id="reader" style="width:500px;height:450px; margin: 0 auto;">
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$('#reader').html5_qrcode(function(data){
			window.location = data;
		},
		function(error) {
			console.log(error);
		},
		function(videoError){
			alert(videoError);
		}
	);
</script>

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
		var u=(("https:" == document.location.protocol) ? "https" : "http") + "://gymmt.sk/piwik/";
		_paq.push(["setTrackerUrl", u+"piwik.php"]);
		_paq.push(["setSiteId", "2"]);
		var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
		g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Piwik Code -->