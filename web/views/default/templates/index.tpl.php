<section class="row">
	<div class="large-10 large-centered columns">
		<ul data-orbit>
			<li>
				<img src="{siteurl}/views/{defaultView}/images/banners/responzive.png" alt="Responzivny banner" style="margin: 0 auto"/>
			</li>
			<li>
				<img src="{siteurl}/views/{defaultView}/images/banners/qrcode.png" alt="Responzivny banner" style="margin: 0 auto"/>
			</li>
		</ul>
	</div>
</section>

<section class="row">
	<div class="large-10 large-centered columns">
		<form name="formSearch" id="formSearch" method="get" action="{siteurl}/search/">
			<div class="row">
				<div class="large-10 large-centered columns">
					<div class="row">
						<div class="large-10 columns">
							<input type="text" id="searchQuery" name="searchQuery" placeholder="Sem zadajte knihu alebo autora" required style="font-size: x-large; padding: 8px; color: rgba(0,0,0,0.75);"/>
						</div>
						<div class="large-2 columns text-center">
							<button class="button small" style="margin-top: 8px;">Vyhľadať</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="text-center">
			<p>Databáza momentálne obsahuje {numBooks} kníh</p>
			<p><a href="https://docs.google.com/spreadsheet/ccc?key=0AtkRCisQ7yCMdFNpMHBMUFhvN2FMa0ZUcWZpTm1XTEE&usp=sharing" target="_blank">Zoznam kníh v systéme</a> </p>
		</div>
	</div>
</section>

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
		var u=(("https:" == document.location.protocol) ? "https" : "http") + "://gymmt.sk/piwik/";
		_paq.push(["setTrackerUrl", u+"piwik.php"]);
		_paq.push(["setSiteId", "2"]);
		var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
		g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Piwik Code -->
