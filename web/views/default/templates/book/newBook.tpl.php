<section class="row">
	<div class="large-12 columns">
		<div class="row">
			<header><h2>Pridať knihu</h2></header>
		</div>
		<div class="row">
			<div class="large-12 columns" style="padding-top: 20px">
				<form action="{siteurl}/library/book/new" name="formNewBook" id="formNewBook" enctype="multipart/form-data" method="post">
					<div class="row">
						<div class="large-5 columns">
							<label>
								<span>Názov knihy:</span>
								<input type="text" placeholder="Názov knihy" id="newBook_title" name="newBook_title" required/>
							</label>
						</div>

						<div class="large-5 columns">
							<label>
								<span>Autor: </span> <span id="authorNameStatus">zatiaľ ste nevybrali žiadneho autora</span>
								<input type="text" list="authors" placeholder="Autor knihy" id="newBook_author" name="newBook_author" required/>
							</label>
							<datalist id="authors">
								{authorList}
							</datalist>
						</div>
					</div>

					<div class="row">
						<div class="large-6 columns">
							<label>
								<span>ISBN:</span>
								<input type="text" placeholder="ISBN" id="newBook_isbn" name="newBook_isbn" required/>
							</label>
						</div>

						<div class="large-5 columns">
							<label>
								<span>Počet kníh:</span>
								<input type="number" placeholder="Počet kníh" id="newBook_quantity" name="newBook_quantity" value="1" required/>
							</label>
						</div>
					</div>

					<div class="row">
						<div class="large-6 columns">
							<label>
								<span>Obal knihy (nepovinné):</span>
								<input type="file" id="newBook_file" name="newBook_file" class="button small" style="margin-top: 10px;"/>
							</label>
						</div>
						<div class="large-6 columns">

						</div>
					</div>
					<div class="row">
						<div class="small-3 small-centered columns">
							<button type="submit">Odoslať</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
		var u=(("https:" == document.location.protocol) ? "https" : "http") + "://gymmt.sk/piwik/";
		_paq.push(["setTrackerUrl", u+"piwik.php"]);
		_paq.push(["setSiteId", "2"]);
		var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
		g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Piwik Code -->