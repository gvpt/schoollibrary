<section class="row">
	<div class="large-12 columns">
		<div class="row">
			<div class="large-4 columns">
				<img src="{siteurl}/uploads/books/{bookFile}.png" alt="{bookTitle}" class="hide-for-medium-down"/>
			</div>

			<div class="large-8 columns">
				<header style="margin-bottom: 20px;"><h2>{bookTitle}</h2></header>
				<ul class="inline-list">
					<li>
						<a href="{siteurl}/library/author/{bookAuthorUrl}" style="display: inline-block; line-height: 24px;">
							<img src="{siteurl}/views/default/images/author.png" style="height: 16px; width: auto;"/>
							{bookAuthor}
						</a>
					</li>
					<li>
						<span style="display: inline-block; line-height: 24px;">
							<img src="{siteurl}/views/default/images/isbn.png" style="height: 16px; width: auto;"/>
							ISBN: {bookISBN}
						</span>

					</li>
				</ul>
				<table style="width: 100%;">
					<thead>
					<tr>
						<th>Čitateľ</th>
						<th>Dátum požičania</th>
						<th>Dátum vrátenia</th>
						<th>Status</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td colspan="4" class="text-center">Prázdne</td>
					</tr>
					</tbody>
				</table>
				<div class="text-center {onlyLoggedIn}">
					<form action="{siteurl}/borrow/{bookId}" method="get" name="formBorrow" id="formBorrow">
						<button class="button">Rezervovať knihu</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
		var u=(("https:" == document.location.protocol) ? "https" : "http") + "://gymmt.sk/piwik/";
		_paq.push(["setTrackerUrl", u+"piwik.php"]);
		_paq.push(["setSiteId", "2"]);
		var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
		g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Piwik Code -->