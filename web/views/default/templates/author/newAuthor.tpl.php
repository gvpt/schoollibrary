<section class="row">
	<div class="large-12 columns">
		<div class="row">
			<header><h2>Pridať autora</h2></header>
		</div>
		<div class="row">
			<div class="large-12 columns" style="padding-top: 20px">
				<form action="{siteurl}/library/author/new" name="formNewAuthor" id="formNewAuthor" enctype="multipart/form-data" method="post">
					<div class="row">
						<div class="large-6 columns">
							<label>
								<span>Krstné a stredné meno autora:</span>
								<input type="text" placeholder="Krstné a stredné meno autora" id="newAuthor_firstName" name="newAuthor_firstName" required/>
							</label>
						</div>
						<div class="large-6 columns">
							<label>
								<span>Priezvisko autora:</span>
								<input type="text" placeholder="Priezvisko autora" id="newAuthor_lastName" name="newAuthor_lastName" required/>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<label for="newAuthor_file">
								<span>Portrét/fotka autora (nepovinné):</span>
								<input type="file" id="newAuthor_file" name="newAuthor_file" class="button small" style="margin-top: 10px;/>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="small-3 small-centered columns">
							<button type="submit">Odoslať</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
		var u=(("https:" == document.location.protocol) ? "https" : "http") + "://gymmt.sk/piwik/";
		_paq.push(["setTrackerUrl", u+"piwik.php"]);
		_paq.push(["setSiteId", "2"]);
		var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
		g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Piwik Code -->