<section class="row">
	<div class="large-12 columns">
		<div class="row">
			<div class="large-12 columns">
				<header style="margin-bottom: 20px;"><h2>{authorName}</h2></header>
			</div>
		</div>
		<div class="row">
			<div class="large-3 columns">
				<img src="{siteurl}/uploads/authors/{authorFile}.png" alt="{authorName}" class="hide-for-medium-down"/>
				<p style="line-height: 1.4em; margin-top: 10px;" class="hide-for-medium-down">
					Počet kníh v knižnici: {authorBooks} <br />
					<a href="https://sk.wikipedia.org/wiki/{authorWiki}" style="display: inline-block; line-height: 24px;" target="_blank">
						<img src="{siteurl}/views/default/images/wikipedia.png" style="height: 20px; width: auto;"/>
						{authorName}
					</a>
				</p>
			</div>

			<div class="large-9 columns">
				<header><h3>Knihy autora v knižnici</h3></header>
				<table style="width: 100%; margin-top: 10px;">
					<thead>
						<tr>
							<th>Názov knihy</th>
						</tr>
					</thead>
					<tbody>
						{bookList}
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
		var u=(("https:" == document.location.protocol) ? "https" : "http") + "://gymmt.sk/piwik/";
		_paq.push(["setTrackerUrl", u+"piwik.php"]);
		_paq.push(["setSiteId", "2"]);
		var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
		g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Piwik Code -->