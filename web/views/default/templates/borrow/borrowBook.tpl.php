<section class="row">
	<div class="large-12 columns">
		<div class="row">
			<div class="large-12 columns">
				<header><h2>Požičať knihu</h2></header>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<form method="post" action="{currentUrl}" name="formBorrowBook" id="formBorrowBook">
					<div class="row">
						<div class="large-4 columns">
							<img src="{siteurl}/uploads/books/{bookFile}.png" alt="{bookTitle}" class="hide-for-medium-down"/>
						</div>


						<div class="large-8 columns">
							<div class="row">
								<div class="large-12 columns">
									<ul class="no-bullet">
										<li>Titul: <strong>{bookTitle}</strong></li>
										<li><br /></li>
										<li>Autor: <strong>{bookAuthor}</strong></li>
									</ul>
									<hr />
								</div>
							</div>

							<div class="row">
								<div class="large-12 columns">
									<label>
										<span class="hide-for-medium-up">Od:</span>
										<input type="text" placeholder="Požičať od" id="borrowBook_from" name="borrowBook_from" required/>
									</label>
								</div>
							</div>

							<div class="row">
								<div class="large-12 columns">
									<label>
										<span class="hide-for-medium-up">Do:</span>
										<input type="text" placeholder="Požičať do" id="borrowBook_to" name="borrowBook_to" required/>
									</label>
								</div>
							</div>

							<div class="row">
								<div class="small-3 small-centered columns">
									<button type="submit">Odoslať</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">
	$(function() {
		$("#borrowBook_from").datepicker({
			dateFormat: "dd.mm.yy",
			firstDay: 1
		});
		$("#borrowBook_to").datepicker({
			dateFormat: "dd.mm.yy",
			firstDay: 1
		});
	});

	<!-- Piwik -->
	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
		var u=(("https:" == document.location.protocol) ? "https" : "http") + "://gymmt.sk/piwik/";
		_paq.push(["setTrackerUrl", u+"piwik.php"]);
		_paq.push(["setSiteId", "2"]);
		var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
		g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
	<!-- End Piwik Code -->
</script>
