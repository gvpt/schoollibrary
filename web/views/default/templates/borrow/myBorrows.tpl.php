<section class="row">
	<div class="large-12 columns">
		<header><h2>Moje zapožičané knihy</h2></header>
		<table style="width: 100%; margin-top: 20px;">
			<thead>
			<tr>
				<th>Titul</th>
				<th>Autor</th>
				<th>Požičaná od</th>
				<th>Požičaná do</th>
				<th>Status</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td colspan="5" class="text-center">Prázdne</td>
			</tr>
			</tbody>
		</table>
	</div>
</section>

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
		var u=(("https:" == document.location.protocol) ? "https" : "http") + "://gymmt.sk/piwik/";
		_paq.push(["setTrackerUrl", u+"piwik.php"]);
		_paq.push(["setSiteId", "2"]);
		var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
		g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Piwik Code -->