<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>{title}</title>
	<meta name="robots" content="noindex, nofollow" />
	<meta name="author" content="Jakub Dubec" />
	<meta name="viewport" content="width=device-width" />
	
	<!-- CSS -->
	<link rel="stylesheet" href="{siteurl}/views/{defaultView}/css/app.css" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="{siteurl}/views/{defaultView}/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="{siteurl}/views/{defaultView}/images/favicon.ico" type="image/x-icon">
		
	<!-- JS -->
	<script src="{siteurl}/views/{defaultView}/js/vendor/jquery.js"></script>
	<script src="{siteurl}/views/{defaultView}/js/vendor/modernizr.js"></script>
	<script src="{siteurl}/views/{defaultView}/js/html5ext.js"></script>
	<script src="{siteurl}/views/{defaultView}/js/html5-qrcode.min.js"></script>
	<script src="{siteurl}/views/{defaultView}/js/jquery-ui.min.js"></script>
    <script src="{siteurl}/views/{defaultView}/js/scripts.js"></script>

	<!--[if lt IE 8]><script type="text/javascript">alert("Your browser is obsolete, please use Mozilla Firefox!");</script><![endif]-->
	<!--[if IE]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	<meta property="og:title" content="{title}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{siteurl}" />
	<meta property="og:image" content="http://gymmt.sk/kniznica/views/default/images/logoFacebook.png" />
	<meta property="og:site_name" content="Digitálna knižnica - GVPT" />
	<meta property="og:description" content="Digitálna knižnica školy Gymnázium Viliama Paulinyho - Tótha v Martine, ktorá umožnuje jednoduchú rezerváciu kníh." />

	<script>
		var _paq = _paq || [];
		_paq.push(['setCustomVariable',
			1,
			"UserName",
			"{userFullName}",
			"visit"
		]);
	</script>

</head>
<body>
	{header}
	{content}
	<footer id="footer">
		<div class="row">
			<div class="large-3 columns">
			    <p class="text-right white"><small style="font-size: x-small">by <a href="http://jakubdubec.me" target="_blank" rel="author">Jakub Dubec</a> &amp; <a href="https://plus.google.com/102160564908273311664/about" target="_blank">Žofia Žiaková</a> &copy; 2014</small></p>
            </div>
			<div class="large-9 columns">
				<!--
                <ul class="inline-list text-left">
                    <li><a href="{siteurl}/about">O projekte</a></li>
                    <li><a target="_blank" href="{siteurl}/about/blog">Blog &amp; Novinky</a></li>
                    <li><a href="{siteurl}/about/bug">Nahlásiť chybu</a></li>
                </ul>
                -->
			</div>
		</div>
	</footer>
    <script src="{siteurl}/views/{defaultView}/js/foundation.min.js"></script>
    <script>
		$(document).foundation({
			orbit: {
				animation: 'slide',
				timer_speed: 5000,
				pause_on_hover: true,
				resume_on_mouseout: true,
				animation_speed: 500,
				navigation_arrows: false,
				bullets: false,
				slide_number: false
			}
		});
	</script>
	<!-- This page was generated with my custom PHP framework -->
</body>
</html>