<?php
/**
 * User: admin
 * Date: 17.2.2014
 * Time: 16:33
 */

class Authors {

	private $registry;

	public function __construct(Registry $registry) {
		$this->registry = $registry;
	}

	public function listAuthors() {
		$cache = $this->registry->getObject('db')->cacheQuery("SELECT id_author, CONCAT(aut_firstName,' ',aut_lastName) AS fullName FROM author ORDER BY aut_lastName ASC");
		return $cache;
	}
}