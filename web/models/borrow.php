<?php
/**
 * User: admin
 * Date: 25.3.2014
 * Time: 21:20
 */

class Borrow {

	private $registry;

	private $id;
	private $bookId;
	private $bookTitle;
	private $authorName;
	private $authorId;
	private $userId;
	private $period;
	private $status;

	public function __construct(Registry $registry, $id = 0) {
		$this->registry = $registry;
		if ($id > 0) {
			$this->registry->getObject('db')->executeQuery("SELECT * FROM vwBorrow WHERE id_borrow = $id");
			if ($this->registry->getObject('db')->numRows() == 1) {
				$row = $this->registry->getObject('db')->getRows();
				$this->id = $id;
				$this->bookTitle = $row['bok_title'];
				$this->bookId = $row['id_book'];
				$this->authorId = $row['id_author'];
				$this->authorName = $row['authorFullName'];
				$this->userId = $row['id_user'];
				$this->status = $row['brw_status'];
				$this->period['from'] = new DateTime($row['brw_from']);
				$this->period['to'] = new DateTime($row['brw_to']);
				$this->valid = true;
			}
			else {
				$this->valid = false;
			}
		}
		else {
			$this->valid = false;
			$this->id = 0;
		}
	}

	public function isValid() {
		return $this->valid;
	}

	public function toArray() {
		$result = array();
		foreach($this as $field => $data) {
			if(!is_object($data) && !is_array($data)) {
				$result[$field] = $data;
			}
		}
		return $result;
	}

	public function setPeriodFrom($value) {
		$this->period['from'] = DateTime::createFromFormat("d.m.Y", $value);
	}

	public function setPeriodTo($value) {
		$this->period['to'] = DateTime::createFromFormat("d.m.Y", $value);
	}

	public function setStatus($value) {
		$this->status = $value;
	}

	public function setBookId($value) {
		$this->bookId = intval($value);
	}

	public function save() {
		if ($this->id > 0) {
			$update = array();
			$update['brw_from'] = $this->period['from']->format("Y-m-d");
			$update['brw_to'] = $this->period['to']->format("Y-m-d");
			$update['brw_status'] = $this->status;
			return $this->registry->getObject('db')->updateRecords('borrow', $update, 'id_borrow=' . $this->id);
		}
		else {
			$insert = array();
			$insert['id_book'] = $this->bookId;
			$insert['brw_from'] = $this->period['from']->format("Y-m-d");
			$insert['brw_to'] = $this->period['to']->format("Y-m-d");
			$insert['brw_status'] = $this->status;
			$insert['id_user'] = $this->registry->getObject('auth')->getUser()->getId();
			if ($this->registry->getObject('db')->insertRecords('book', $insert)) {
				$this->id = $this->registry->getObject('db')->lastInsertID();
				return true;
			}
			else {
				return false;
			}
		}
	}
}