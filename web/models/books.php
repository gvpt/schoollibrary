<?php
/**
 * User: admin
 * Date: 18.2.2014
 * Time: 16:09
 */

class Books {

	private $registry;

	public function __construct(Registry $registry) {
		$this->registry = $registry;
	}

	public function listBooksByAuthor($authorId) {
		$cache = $this->registry->getObject('db')->cacheQuery("SELECT bok_title, bok_url FROM book WHERE id_author = $authorId");
		return $cache;
	}

	public function numBooks() {
		$this->registry->getObject('db')->executeQuery("SELECT COUNT(id_book) AS numBooks FROM book");
		$row = $this->registry->getObject('db')->getRows();
		return $row['numBooks'];
	}
}