<?php
/**
 * User: admin
 * Date: 12.2.2014
 * Time: 20:53
 */

class Book {

	private $registry;

	private $id;
	private $title;
	private $authorId;
	private $authorName;
	private $isbn;
	private $quantity;

	private $valid;

	public function __construct(Registry $registry, $id = 0) {
		$this->registry = $registry;
		if ($id > 0) {
			$this->registry->getObject('db')->executeQuery("SELECT * FROM vwBook WHERE id_book = $id");
			if ($this->registry->getObject('db')->numRows() == 1) {
				$row = $this->registry->getObject('db')->getRows();
				$this->id = $id;
				$this->title = $row['bok_title'];
				$this->authorId = $row['id_author'];
				$this->authorName = $row['aut_firstName'] . ' ' . $row['aut_lastName'];
				$this->isbn = $row['bok_isbn'];
				$this->quantity = $row['bok_quantity'];
				$this->valid = true;
			}
			else {
				$this->valid = false;
			}
		}
		else {
			$this->valid = false;
			$this->id = 0;
		}
	}

	public function isValid() {
		return $this->valid;
	}

	public function toArray() {
		$result = array();
		foreach($this as $field => $data) {
			if(!is_object($data) && !is_array($data)) {
				$result[$field] = $data;
			}
		}
		return $result;
	}

	public function setTitle($value) {
		$this->title = $this->registry->getObject('db')->sanitizeData(trim($value));
	}

	public function setAuthor($value) {
		$this->authorId = intval($value);
	}

	public function setISBN($value) {
		$this->isbn = $this->registry->getObject('db')->sanitizeData(trim($value));
	}

	public function setQuantity($value) {
		$this->quantity = intval($value);
	}

	public function save() {
		if ($this->id > 0) {
			$update = array();
			$update['id_author'] = $this->authorId;
			$update['bok_title'] = $this->title;
			$update['bok_isbn'] = $this->isbn;
			$update['bok_quantity'] = $this->quantity;
			$update['bok_url'] = formatHelper::createUrlString($this->title);
			return $this->registry->getObject('db')->updateRecords('book', $update, 'id_book=' . $this->id);
		}
		else {
			$insert = array();
			$insert['id_author'] = $this->authorId;
			$insert['bok_title'] = $this->title;
			$insert['bok_isbn'] = $this->isbn;
			$insert['bok_quantity'] = $this->quantity;
			$insert['bok_url'] = formatHelper::createUrlString($this->title);
			if ($this->registry->getObject('db')->insertRecords('book', $insert)) {
				$this->id = $this->registry->getObject('db')->lastInsertID();
				return true;
			}
			else {
				return false;
			}
		}
	}
}