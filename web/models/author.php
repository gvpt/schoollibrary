<?php
/**
 * User: admin
 * Date: 13.2.2014
 * Time: 20:57
 */

class Author {

	private $registry;

	private $id;
	private $firstName;
	private $lastName;
	private $url;
	private $cover;

	private $valid;

	public function __construct(Registry $registry, $id = 0) {
		$this->registry = $registry;
		if ($id > 0) {
			$this->registry->getObject('db')->executeQuery("SELECT * FROM author WHERE id_author = $id");
			if ($this->registry->getObject('db')->numRows() == 1) {
				$row = $this->registry->getObject('db')->getRows();
				$this->id = $id;
				$this->firstName = $row['aut_firstName'];
				$this->lastName = $row['aut_lastName'];
				$this->url = $row['aut_url'];
				$this->cover = formatHelper::createFileName($row['aut_firstName'] . $row['aut_lastName']);
				$this->valid = true;
			}
			else {
				$this->valid = false;
			}
		}
		else {
			$this->id = 0;
			$this->valid = false;
		}
	}

	public function isValid() {
		return $this->valid;
	}

	public function toArray() {
		$result = array();
		foreach($this as $field => $data) {
			if(!is_object($data) && !is_array($data)) {
				$result[$field] = $data;
			}
		}
		return $result;
	}

	public function setFirstName($value) {
		$this->firstName = $this->registry->getObject('db')->sanitizeData(trim($value));
	}

	public function setLastName($value) {
		$this->lastName = $this->registry->getObject('db')->sanitizeData(trim($value));
	}

	public function getFullName() {
		return $this->firstName . ' ' . $this->lastName;
	}

	public function save() {
		if ($this->id > 0) {
			$update = array();
			$update['aut_firstName'] = $this->firstName;
			$update['aut_lastName'] = $this->lastName;
			$update['aut_url'] = formatHelper::createUrlString($this->firstName . '+' .$this->lastName);
			return $this->registry->getObject('db')->updateRecords('author', $update, 'id_author=' . $this->id);
		}
		else {
			$insert = array();
			$insert['aut_firstName'] = $this->firstName;
			$insert['aut_lastName'] = $this->lastName;
			$insert['aut_url'] = formatHelper::createUrlString($this->firstName . '+' .$this->lastName);
			if ($this->registry->getObject('db')->insertRecords('author', $insert)) {
				$this->id = $this->registry->getObject('db')->lastInsertID();
				return true;
			}
			else {
				return false;
			}
		}
	}

	public function numBooks() {
		$this->registry->getObject('db')->executeQuery("SELECT COUNT(id_book) as numBooks FROM book WHERE id_author = " . $this->id);
		$row = $this->registry->getObject('db')->getRows();
		return $row['numBooks'];
	}
} 