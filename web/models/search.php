<?php
class Search {
	private $registry;
	
	public function __construct(Registry $registry) {
		$this->registry = $registry;
	}
	
	public function searchRecords($q, $offset = 0) {
		require_once(FRAMEWORK_PATH . 'libs/pagination/pagination.class.php');
		$paginatedMembers = new Pagination($this->registry);
		$paginatedMembers->setLimit(16);
		$paginatedMembers->setOffset($offset);
		$query = "SELECT * FROM vwSearch WHERE bok_title LIKE '%$q%' OR aut_fullName LIKE '%$q%'";
		$paginatedMembers->setQuery($query);
		$paginatedMembers->setMethod('cache');
		$paginatedMembers->generatePagination();
		return $paginatedMembers;
	}
}
?>